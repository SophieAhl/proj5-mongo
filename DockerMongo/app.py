import os
import flask
import acp_times_solved
import arrow
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route('/')
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.route("/_calc_times")
def _calc_times():
    _items = db.tododb.find()
    items = [item for item in _items]

    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 200, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    tm = request.args.get('tm', None, type=str)
    app.logger.debug("tm={}".format(tm))
    br = request.args.get('br', None, type=int)
    dt = request.args.get('dt', None, type=str)

    a_time = arrow.get(dt + " " + tm, "YYYY-MM-DD HH:mm")
    # a_date = arrow.get(dt, "")

    app.logger.debug("a_time={}".format(a_time))

    open_time = acp_times_solved.open_time(km, br, a_time.isoformat())
    close_time = acp_times_solved.close_time(km, br, a_time.isoformat())
    result = {"open": open_time, "close": close_time}

    app.logger.debug("result:" + str(result))

    return flask.jsonify(result=result)

@app.route('/display')
def display():
    items = [item for item in db.tododb.find()] 
    return render_template('todo.html', items=items)

@app.route('/new', methods=['POST'])

def new():
    open_DATA = flask.request.form.getlist("open")
    close_DATA = flask.request.form.getlist("close")
    km_DATA = flask.request.form.getlist("km")

    app.logger.debug(flask.request.form)

    app.logger.debug("open_DATA"+ str(open_DATA))
    app.logger.debug("close_DATA"+ str(close_DATA))
    app.logger.debug("km_DATA"+ str(km_DATA))

    OPEN_LIST = []
    CLOSED_LIST = []
    KM_LIST = []

    for x in km_DATA:
        if str(x)!='':
            KM_LIST.append(str(x)) 
    for x in open_DATA:
        if str(x)!='':
            OPEN_LIST.append(str(x)) 
    for x in close_DATA:
        if str(x)!='':
            CLOSED_LIST.append(str(x)) 

    app.logger.debug("OPEN " + str(OPEN_LIST))
    app.logger.debug("CLOSE " + str(CLOSED_LIST))
    app.logger.debug("KM" + str(KM_LIST))

    k = len(KM_LIST)

    app.logger.debug("DATA LENGTH"+ str(k))


    for i in range(k):
        app.logger.debug("HELLO!!!" + str(i))
        print("KM"+ KM_LIST[i], "OPEN "+ OPEN_LIST[i], "CLOSE"+ CLOSED_LIST[i])
        item_doc = {
            'km': KM_LIST[i],
            'open_time': OPEN_LIST[i],
            'close_time': CLOSED_LIST[i]
        }
        db.tododb.insert_one(item_doc)

    _items = db.tododb.find() 
    items = [item for item in _items]

    if items!=[]:
        return redirect(url_for('index'))


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
