"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    MAXSPEED_T = [(0, 34), (200, 34), (400, 32), (600, 30), (1000, 28)]

    n = 0
    time = 0

    for n in range (len(MAXSPEED_T)):
      if control_dist_km >= MAXSPEED_T[n][0]:
        km = control_dist_km - MAXSPEED_T[n][0]
        time_per_dist = km/MAXSPEED_T[n][1]
        time += time_per_dist
        control_dist_km -= km
          

    brevet_start_time = arrow.utcnow()
    hours = int(time)
    minutes = round(60*(time-hours))
    time_at_opening = brevet_start_time.shift(hours =+ hours, minutes =+ minutes)
    return time_at_opening.isoformat()
    #return arrow.now().isoformat()
    #Source: https://arrow.readthedocs.io/en/latest/


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    MINSPEED_T = [(0, 15),(200, 15), (400, 15), (600, 15), (1000, 11.428)]

    n = 0
    time = 0

    for n in range (len(MINSPEED_T)):
      if control_dist_km >= MINSPEED_T[n][0]:
        km = control_dist_km - MINSPEED_T[n][0]
        time_per_dist = km/MINSPEED_T[n][1]
        time += time_per_dist
        control_dist_km -= km
        

    brevet_start_time = arrow.utcnow()
    hours = int(time)
    minutes = round(60*(time-hours))
    time_at_opening = brevet_start_time.shift(hours =+ hours, minutes =+ minutes)
    return time_at_opening.isoformat()

    return time_at_closing.isoformat()
    #return arrow.now().isoformat()
