# Project 5: Brevet time calculator with Ajax and MongoDB

By: Sophie Ahlberg
Contact: sahlberg@uoregon.edu

This project reimplements the RUSA ACP controle time calculator, featuring flask,  ajax and testing.

Credits to Michal Young for the initial version of this code.

This project is meant  to calculate the open times and the closed times in a similar style to that of the RUSA ACP controle time calculator. As of now, the calculator receives information from two tables, one for closed time and one for open time, located in the python document acp_times.py. The calculator takes the input of kilometers which is given by the user, turns it into miles, and uses the two tables to determine what the open and closed time will be. It also uses the total distance drop-down menu to determine what the assigned open and closed time will be.

Furthermore, there is a submit button which the user presses to determine the kilometers added to the table, and a Display button, which takes the user to a new page that displays all of the entered kilometers and their assigned open and closed times, depending on what the total brevet distance is. However, as of now, the code is not finished and no testcases have been added. There seems to be an issue with the Open and Closed times since they will not be added to a list. There also seems to be an error with the connection of MongoDB. 
